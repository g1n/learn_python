input_list = [9,3,4,1,5,7,6,2,8,10]
for i in range(0, len(input_list)-1):
    for j in range(0, len(input_list)-1):
        if j < len(input_list)-i:
           value = input_list[j]
           next_value = input_list[j+1]
           if value > next_value:
               input_list[j] = next_value
               input_list[j+1] = value
        else:
           value = input_list[j]
           prev_value = input_list[j-1]
           if value < prev_value:
               input_list[j] = prev_value
               input_list[j-1] = value

print(input_list)           
